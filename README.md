# PROGRAMACION LOGICA Y FUNCIONAL

## Lenguajes de programación y algoritmos
### 1. Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startmindmap
*[#Orange]:Programando ordenadores
en los 80 y ahora.;
**[#lightgreen] Sistema antiguo.
***_ es
****:Cualquier sistema que ya no
este a la venta.;
***** No hay continuidad.
*****_ Existian en
****** Ordenadores de los 80's y 90's.
*******_ con
******** Menor potencia.
*******_ Tipo de lenguaje
******** Ensamblador.
*********_ Para ello
**********:Se tenia que conocer a fondo 
la arquitectura de la máquina.;
**[#FFBBCC] Sistema actual
***_ Se utiliza
**** Lenguaje de alto nivel
***** Programas complejos
****** C
****** Java
****** C++
***_ Desventajas
****:Sobrecarga en 
tiempo de ejecución;
*****_ Abstracción
******:El programador
no conoce el hardware;
*****_ Ineficiencia
******:Muchas 
actualizaciones;
**[#lightblue] Ley de Moore
***_ es
**** Ley empírica
*****_:Menciona
que;
******:Cada 18 meses el número
de componentes que se puede meter
en un circuito impreso 
se duplica.;
*******_:Menciona
que;
********:Cada 18 meses se puede
generar una cpu
el doble de rapida.;

*****_ Limitaciones
****** Físicas

**[#D4BDF9] Ley de wirth
***_:Menciona
que;
****:El sofware se vuelve
el doble de lento 
compensando la Ley de Moore.;
@endmindmap 

```
### 2. Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml

@startmindmap
*[#Orange]:Historia de los algoritmos y
de los lenguajes de programación.;

**[#B0F25F] Algoritmo
***_ Es
****:Una lista bien definida
ordenada y finita de 
instrucciones.;
*****:Permite encontrar la 
solución a un problema.;
****** No surgen de las computadoras.
****** No da lugar a ambigüedades.
*****_ Contiene
****** Entrada.
****** Proceso.
****** Salida.
*****_ Se divide en
******[#DEF9BD] Polinomiales
*******:Crece despacio
a medida que crece
el problema.;

******[#DEF9BD] Exponenciales
*******:Se duplica cada
que crece el problema.;

**[#B0F25F]:Lenguajes de
programación.;
***_ Son
****:Instrumentos
apropiados.;
*****_ Para
******:Comunicar los algoritmos
a las máquinas
que lo ejecutan.;
***_ Son
****:Paradigmas o familias
de lenguajes.;
*****_ Como son
******[#4BFEB8] Imperativo
******[#4BFEB8] Funcional
*******_ Empleados en
******** Empresas con alto contenido tecnólogico
*********_ Aportan
********** Menor líneas de código
******[#4BFEB8] Programacion lógica
*******_ Empleados en
******** Empresas con alto contenido tecnólogico
*********_ Aportan
********** Menor líneas de código
******[#4BFEB8] POO
******[#4BFEB8] Programas paralelos.

@endmindmap
```

### 3. Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#Orange]:Tendencias actuales en los
lenguajes de programación y Sistemas Informáticos.
La evolución de los lenguajes y 
paradigmas de programación.;
**[#BFFDCF] Lenguajes de programación
***[#F0FC56] Evolución
****:Cada vez se buscan
paradigmas más abstractos.;
****:Que se acerquen a la manera
natural de expresarnos.;
*****_:¿Porque surgen
nuevos paradigmas?;
******:Crisis en 
la programación.;

***_ Es un
****:Medio de comunicación
entre el hombre 
y la máquina.;
*****_ Tienen que
******:Adaptarse a las
crecientes
necesidades.;
******:Ofrecer eficiencia
en la ejecución.;

**[#BFFDCF] Paradigmas de programación
***_ Son una
****:Forma de aproximarse
a la solución de un 
programa.;
***_ Como son
****[#D2BFFD] Estructurado
*****:No hay dependencias entre
una determinada arquitectura
de hardware.;
******_ Ejemplos
******* C
******* Fortran
******* Basic
******* Pascal
****[#D2BFFD] Funcional
*****:Usar el lenguaje
de las matemáticas.;
****** Funciones matemáticas
****[#D2BFFD] Lógico
*****_ Se basa en
****** Expresiones lógicas
******* Predicados lógicos
*******_ Objetivo
******** Modelizar un problema
*******_ Ejemplo
******** Lenguaje Prolog
****[#D2BFFD] Orientado a objetos
*****:Organiza el diseño de software
en torno a 
datos u objetivos.;
******_ Java
*******:Nueva manera de organizar el
código de un programa.;
****[#D2BFFD] Orientada a aspectos
*****:Si los aspectos pueden programarse
separadamente y mantenerse 
separadamente y solamente al final del 
proyecto integrarse.;
****[#D2BFFD]:Basada en
componentes.;
*****_ Componente
******:Conjunto de objetos que nos da 
una funcionalidad mucho mayor.;
*****:Lleva más lejos el 
paradigma de los objetos.;
******_ Con el fin de
******* Dar una mayor utilización a estos
****[#D2BFFD]:Orientada a agente
software.;
*****_ Son
******:Aplicaciones informaticas con capacidad para
decidir cómo deben actuar para alcanzar sus objetivos.;
****[#D2BFFD] Multiagente
*****:Los agentes deben de tener una habilidad
social para la interacción con otros agentes.;
****[#D2BFFD] Distribuida
*****_ Surge para que
******:Los ordenadores se conecten
entre sí.;
****[#D2BFFD] Concurrente
*****_ Pretende
****** Dar una solución cuando multitud de usuarios acceden simultáneamente a un programa. 

@endmindmap


```
